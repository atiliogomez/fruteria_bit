package atiliogomez.com.bit.fruteriabit;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import atiliogomez.com.bit.fruteriabit.daos.FrutaDao;
import atiliogomez.com.bit.fruteriabit.database.AppDatabase;
import atiliogomez.com.bit.fruteriabit.entities.Fruta;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class FrutaTest {
    private FrutaDao frutaDao;
    private AppDatabase appDatabase;

    @Before
    public void createDB(){
        Context context = ApplicationProvider.getApplicationContext();
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        frutaDao = appDatabase.frutaDao();
    }

    @After
    public void closeDB() throws IOException{
        appDatabase.close();
    }

    @Test
    public void findByNameTest() throws Exception{
        Fruta fruta = new Fruta();
        fruta.setId(1);
        fruta.setNombre("Naranja");

        frutaDao.insert(fruta);

        Fruta buscada = frutaDao.findByNombre("Naranja");

        assertTrue("No encontré la naranja que tenía", fruta.getId() == buscada.getId());
    }
}
