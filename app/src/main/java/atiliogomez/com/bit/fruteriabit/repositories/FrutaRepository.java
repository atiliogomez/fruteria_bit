package atiliogomez.com.bit.fruteriabit.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import atiliogomez.com.bit.fruteriabit.daos.FrutaDao;
import atiliogomez.com.bit.fruteriabit.database.AppDatabase;
import atiliogomez.com.bit.fruteriabit.entities.Fruta;

public class FrutaRepository {
    private FrutaDao frutaDao;

    private LiveData<List<Fruta>> frutas;

    public FrutaRepository(Application application){
        AppDatabase db = AppDatabase.getInstance(application);
        frutaDao = db.frutaDao();
        frutas = frutaDao.getAll();
    }

    public LiveData<List<Fruta>> getFrutas(){
        return frutas;
    }

    public void insert(Fruta fruta){
        AppDatabase.databaseWriteExecutor.execute(()->{
            frutaDao.insert(fruta);
        });

    }
}
