package atiliogomez.com.bit.fruteriabit.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import atiliogomez.com.bit.fruteriabit.entities.Fruta;

@Dao
public interface FrutaDao {

    @Query("SELECT * FROM fruta")
    LiveData<List<Fruta>>  getAll();

    @Insert
    void insert(Fruta fruta);

    @Update
    void update(Fruta fruta);

    @Delete
    void delete (Fruta fruta);

    @Query("SELECT * FROM fruta WHERE nombre LIKE :nombre")
    Fruta findByNombre(String nombre);

    @Query("SELECT * FROM fruta WHERE id = :id")
    Fruta findById(int id);
}
