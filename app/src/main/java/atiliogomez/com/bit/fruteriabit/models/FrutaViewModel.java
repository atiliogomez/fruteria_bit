package atiliogomez.com.bit.fruteriabit.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import atiliogomez.com.bit.fruteriabit.entities.Fruta;
import atiliogomez.com.bit.fruteriabit.repositories.FrutaRepository;

public class FrutaViewModel extends AndroidViewModel {

    private FrutaRepository frutaRepository;
    private final LiveData<List<Fruta>> frutas;

    public FrutaViewModel(Application application){
        super(application);
        frutaRepository = new FrutaRepository(application);
        frutas = frutaRepository.getFrutas();

    }

    public LiveData<List<Fruta>>getFrutas(){
        return frutas;
    }

    public void insert(Fruta fruta){
        frutaRepository.insert(fruta);
    }
}
